# Codewar-Katas

Codewars.com is one of my favorite sites to keep my skills sharp and learn new and better ways to write code through solving code challenges.

This repo is where I post some of my solutions.

All answers in repo were my own solutions.

See all of my completed Codewar Katas(code challenges) at: http://www.codewars.com/users/josh-mcq - (it won't let you see my answers unless you have an account and have solved said Kata)

Kyu levels: 1 Kyu is Expert, 8 Kyu is Beginner.

**did_you_mean (3 Kyu)**
Method that takes an entered term (lowercase string) and an array of known words (also lowercase strings). Returns word from the dictionary that is most similar to the entered one. The similarity is described by the minimum number of letters you have to add, remove or replace in order to get from the entered word to one of the dictionary. The lower the number of required changes, the higher the similarity between each two words.

**molecules_to_atoms(3 Kyu)**
For a given chemical formula represented by a string, count the number of atoms of each element contained in the molecule and return an object.

For example:

water = 'H2O'
parse_molecule(water)                 # return {H: 2, O: 1}

magnesium_hydroxide = 'Mg(OH)2'
parse_molecule(magnesium_hydroxide)   # return {Mg: 1, O: 2, H: 2}

var fremy_salt = 'K4[ON(SO3)2]2'
parse_molecule(fremySalt)             # return {K: 4, O: 14, N: 2, S: 4}
As you can see, some formulas have brackets in them. The index outside the brackets tells you that you have to multiply count of each atom inside the bracket on this index. For example, in Fe(NO3)2 you have one iron atom, two nitrogen atoms and six oxygen atoms.

Note that brackets may be round, square or curly and can also be nested. Index after the braces is optional.

**can you get the loop(3 Kyu)**
You are given a node that is the beginning of a linked list. This list always contains a tail and a loop.

Your objective is to determine the length of the loop.

Use the `next' attribute to get the following node

**recoverSecret (4 Kyu)**
Returns the secret string by evaluating a list of triplets that show the relative order of letters of given letters within the secret string.

**rpn_calculator (4 Kyu)**
Function that evaluates a mathematical expression of Reverse Polish Notation form.

**count_change (4 Kyu)**
Calculates how many ways a given set of denominations can add up to a total amount.

**double cola (5 kyu)**
Calculates which person will receive the Nth soda out of a vending machine that doubles and sends to the back of the line each person as they get a soda.

**rgb_to_hex (5kyu)**
Converts an RGB color value to a hexadecimal color value.

**css_selector_comparison (5 Kyu)**
Compares two different css selectors to determine which has higher priority based on CSS heirarchy.

**sudoku_validator (5 Kyu)**
Validates a sudoku solution to determine if it meets all requirements.

**title_case (6 Kyu)**
Capitalize words in given string unless present in list of specified exception words. First word capitalized even if it's an exception word.

**look_and_say(6 Kyu)**
Recursively returns a description of each previous numerical string such that '31' describes '111'.

**wordify (6 Kyu)**
Takes an integer n, between 1 and 999,	 as argument and returns the english word version, 123 = 'one hundred twenty three'.

**most_frequent_elements (7 Kyu)**
Determines which element(s) appear most frequently in given array.

**Deck of Cards**
While this particular exercise is NOT from Codewars, it sorta fits nicely in this category.

