#!/bin/bash

# Executable which takes as it's first argument a path to a filename containing integer numbers, 
# one per line. Print to stdout, the fibonacci number, F(n)



while read line; do
    i=0;j=1;n=0
    while [ $n -lt $line ]
    do	    
        c=$j;j=$[$i+$j];i=$c;n=$[n+1]
    done
    echo $i
done < "$1"