#!/bin/bash

#A happy number is defined by the following process. 
#Starting with any positive integer, replace the number 
#by the sum of the squares of its digits, and repeat 
#the process until the number equals 1 (where it will stay), 
#or it loops endlessly in a cycle which does not include 1. 
#Those numbers for which this process ends in 1 are happy numbers, 
#while those that do not end in 1 are unhappy numbers.

#Executable file that will take a file with one number per line, and 
#print 1 to output if it is a happy number, otherwise print 0.

sum_of_squares()
for num; do
    i=0; sum=0
    while [ $i -lt ${#num} ];do
        sum=$[sum+$[${num:$i:1}**2]]
        i=$[i+1]
    done
    echo $sum
done

contains() {    
    for x in ${sum_array[@]};do
        if [ $x -eq $val ];then
            echo 1
            return 1
        fi
    done
    echo 0
}



while read arg; do
    x=1; sum_array=($arg); val=$(sum_of_squares $arg)
    while [ $val -ne 1 ];do
        x=$[x+1]
        if [ $(contains $sum_array $val) -eq 1 ]; then
            echo 0
            break
        else
            sum_array[$x]=$val
            val=$(sum_of_squares $val)
        fi
    done
    if [ $val -eq 1 ];then 
        echo 1
    fi
done < $1
    




