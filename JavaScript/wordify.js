function wordify(n) {
  var ints = ['','one','two','three','four','five','six','seven','eight','nine','ten','eleven','twelve','thirteen','fourteen','fifteen','sixteen','seventeen','eighteen','nineteen'];
  var tens = ['', '','twenty','thirty','forty','fifty','sixty','seventy','eighty','ninety'];
  
  
function tencalc(num) {
    if(num<20) {
        return ints[parseInt(num)];
    }    
    else {
        var x = num.toString()
        var tenspot =  tens[x[0]];
        var onespot = ((x[1] != '0') ? ' ' + ints[x[1]]:'');
    }
return tenspot + onespot;
}

if (n.toString().length < 3) {
  return tencalc(n);
}
else {
    var x = n.toString();
    var hundreds = ints[x[0]] + ' hundred';
    var tenspot = ((x[1] != '0') ? ' ' + tens[x[1]]:'');
    var onespot = ((x[2] != '0') ? ' ' + ints[x[2]]:'');
    return hundreds + ((x.slice(-2) != '00') ? ' ' + tencalc(x.slice(-2)):'');
}
}
