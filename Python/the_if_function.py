def _if(bool, func1, func2):
    #import pdb;pdb.set_trace()
    if bool == True:
        return func1()
    else:
        return func2()







from unittest import TestCase, main


class Test(TestCase):
       
    def test__if(self):
        truthy = lambda:"True"
        falsey = lambda:"False"
                
        self.assertEqual(_if(1, truthy, falsey), "True")

    def test__if(self):
        truthy = lambda:"PingPong"
        falsey = lambda:"TableTennis"
                
        self.assertEqual(_if(0, truthy, falsey), "TableTennis")


if __name__ == '__main__':main()