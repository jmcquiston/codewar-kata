"""
Description:

A string is considered to be in title case if each word in the string is either (a) capitalised (that is, only the first letter of the word is in upper case) or (b) considered to be an exception and put entirely into lower case unless it is the first word, which is always capitalised.

Write a function that will convert a string into title case, given an optional list of exceptions (minor words). The list of minor words will be given as a string with each word separated by a space. Your function should ignore the case of the minor words string -- it should behave in the same way even if the case of the minor word string is changed.

Example:

title_case('a clash of KINGS', 'a an the of') # should return: 'A Clash of Kings'
title_case('THE WIND IN THE WILLOWS', 'The In') # should return: 'The Wind in the Willows'
title_case('the quick brown fox') # should return: 'The Quick Brown Fox'
"""

#Solution

def title_case(title, minor_words=" " ):
    def proper_case(word):
        if word.lower() in [w.lower() for w in minor_words.split()]:
            return word.lower()
        return word.title()
            
    title_list = map(proper_case, title.split(" ")[1:])
    title_list.insert(0, title.split(" ")[0].title())
    return " ".join(title_list)



from unittest import TestCase, main


class TestTitleCase(TestCase):

    def test_title_case(self):
        self.assertEqual(title_case('a clash of KINGS', 'a an the of'), 'A Clash of Kings')
        self.assertEqual(title_case('THE WIND IN THE WILLOWS', 'The In'), 'The Wind in the Willows')
        self.assertEqual(title_case('the quick brown fox'), 'The Quick Brown Fox')

if __name__ == "__main__":main()
