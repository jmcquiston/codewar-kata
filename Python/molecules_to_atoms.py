def equals_atomically (obj1, obj2):
    if len(obj1) != len(obj2):
        return False
    for k in obj1:
        if obj1[k] != obj2[k]:
            return False
    return True



import re

def parse_molecule(formula):
    formula = convert_to_brackets(formula) 
    chunked_formula = [i[0] for i in re.findall('(\[|(\]\d+)|\]|[A-Z][a-z]?[\d]*)', formula)]
    elements = {trim(e):0 for e in re.findall('([A-Z][a-z]?\d*)', formula)}                            
    for element in filter(lambda x: x[1][0] not in '[]', enumerate(chunked_formula)):
        rightward = [element[1]]+filter(lambda x: x[0] in '[]', chunked_formula[element[0]:])
        elements[trim(element[1])] += int(element_calc(rightward))
    return {trim(k):v for k,v in elements.items()}

def convert_to_brackets(formula):
    l_brackets, r_brackets = ['(','{'], [')','}']
    for c in formula:
        if c in l_brackets+r_brackets:
            formula = formula.replace(c, '[') if c in l_brackets else formula.replace(c, ']')
    return formula

def trim(k):
    if k[-1] in '123456789':
        i = 0
        while k[i] not in '123456789':
            i+=1
        return k[:i]
    return k

def element_calc(rightward):
    if len(rightward)==1:
        if rightward[0][-1] in '123456789':
            rightward = rightward[0]
            return rightward[rightward.index(trim(rightward)[-1])+1:]            
        else: return 1
    
    level, multiplier = 1,[]
    if rightward[0][-1][-1].isdigit():
        multiplier.append(rightward[0][-1][-1])
    for item in rightward[1:]:
        if item[0] == ']':
            if level>0:
                if len(item)>1:
                    multiplier.append(item[1:])
            else:
                level+=1
        else:
            level-=1
        if not multiplier: multiplier = [1]
    return reduce(lambda a,b: a*b, [int(x) for x in multiplier])


                      



from unittest import TestCase, main

class Test(TestCase):
    def test_parse_molecule(self):
        self.assertTrue(equals_atomically(parse_molecule("H2O"), {'H': 2, 'O' : 1}), "Should parse water")
        self.assertTrue(equals_atomically(parse_molecule("Mg(OH)2"), {'Mg': 1, 'O' : 2, 'H': 2}), "Should parse magnesium hydroxide: Mg(OH)2")
        self.assertTrue(equals_atomically(parse_molecule("K4[ON(SO3)2]2"), {'K': 4,  'O': 14,  'N': 2,  'S': 4}), "Should parse Fremy's salt: K4[ON(SO3)2]2")
        self.assertTrue(equals_atomically(parse_molecule("C6H12O6"), {'C':6, 'H':12, 'O':6}), "bla")
        self.assertTrue(equals_atomically(parse_molecule('[C5H5]Fe[CO]2CH3'), {'H': 8, 'C': 8, 'Fe': 1, 'O': 2}),"fail")
        self.assertTrue(equals_atomically(parse_molecule('{[Co(NH3)4(OH)2]3Co}(SO4)3'),
                                          {'Co': 4, 'N': 12, 'H': 42, 'O': 18, 'S': 3}),
                    "Should parse hexol sulphate: {[Co(NH3)4(OH)2]3Co}(SO4)3")
    def xtest_convert_to_brackets(self):
        formula = "K4{ON(SO3)2}2"
        self.assertEquals(convert_to_brackets(formula), "K4[ON[SO3]2]2")

    def xtest_find_brackets_to_right(self):
        chunked_formula = ['K4', '[', 'O', 'N', '[', 'S', 'O3', ']2', ']2']
        self.assertEquals(find_brackets_to_right(2, chunked_formula), ['[',']2',']2']) # [ ]2]2

    def xtest_element_calc(self):
        rightward = ['K4', '[', 'O', 'N', '[', 'S', 'O3', ']2', ']2']
        self.assertEquals(element_calc(rightward), 4)

    def xtest_element_calc2(self):
        rightward = ['K3', 'O', 'N', '[', 'K', 'O3', ']2', ']2']
        self.assertEquals(element_calc(rightward), 6)
        
if __name__ == "__main__": main()
