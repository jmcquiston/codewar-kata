import re
import operator as op


def calc(expr):
    ''' Calculate the Reverse Polish Notation Expression, return final answer '''
    while ' ' in expr:
        if not re.findall('\+|-|\*|\/',expr):
            return float(re.findall('[\d\.]+', expr)[-1])
        else:
            expr = expr.replace(operation_finder(expr), str(calc_substring(operation_finder(expr))))
    if not expr:
            return 0
    return int(expr)
    
    
def operation_finder(string):
    ''' Parse the expression for any two adjacent numerical values with an operator to the right. '''
    p = re.compile('[\d\.]+\s[\d\.]+\s[\+\*\/-]')
    m = p.search(string)
    return m.group()
    
    
def calc_substring(substring):
    ''' Complete a single operation with two numerical values and a single operator. '''
    op_dict = {'+':op.add, '-':op.sub,'*':op.mul,'/':op.div}
    nums = re.findall(re.compile('[\d\.]+'), substring)
    operator = substring[-1]
    return op_dict[operator](int(nums[0]), int(nums[1]))
    
    


from unittest import TestCase, main

class RPNTest(TestCase):
    def test_expr(self):
        self.assertEqual(calc('5 1 2 + 4 * + 3 -'),14)
        self.assertEqual(calc('1 2 3.5'), 3.5)
        self.assertEqual(calc(''), 0)



if __name__ == "__main__":main()
