def caffeineBubz(x):
   if x%12 == 0:
        result = "Coffee"
    elif x%3 == 0:
        result = "Java"
    else:
        result = ""
        
    if result in ["Coffee", "Java"]:
        if x%2==0:
            return result+"Script"
        else:
            return result
    else:
        return "mocha_missing!"


from unittest import TestCase, main

class test(TestCase):
    def test_caffeineBubz(self):
        self.assertEqual(caffeineBubz(12),"CoffeeScript")

if __name__ == "__main__":main()
