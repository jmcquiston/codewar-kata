class Dictionary:
    def __init__(self,words):
        self.words=words
    def find_most_similar(self,term):
        result = (None,100)
        for word in self.words:
            if find_distance(word, term)<result[1]:
                result = (word, find_distance(word, term))
        return result[0]
        
def find_distance(s1, s2):
    if len(s1) < len(s2):
        return find_distance(s2, s1)

    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)

    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1 # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1       # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row
    
    return previous_row[-1]




from unittest import TestCase, main

class Test(TestCase):
    def test_one(self):
        words=['cherry','peach', 'pineapple', 'melon', 'strawberry']#, 'raspberry', 'apple', 'coconut', 'banana']
        test_dict=Dictionary(words)        
        self.assertEquals(test_dict.find_most_similar('strawbery'),'strawberry')
        self.assertEquals(test_dict.find_most_similar('berry'),'cherry')


if __name__ == "__main__":main()
