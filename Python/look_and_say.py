import itertools
def done_or_not(board): #board[i][j]
  tester = lambda x:len(set(x))==9
  horizontal = all([tester(board[i]) for i in range(9)])
  vertical = all([tester([board[j][i] for j in range(9)]) for i in range(9)])   

   
  
  def squish(r):
      ml = []
      for tri in r:
          for item in tri:
              ml.append(item)
      return ml
      
  

  regions = [
  squish([x[0:3] for x in board[0:3]]),
  squish([x[3:6] for x in board[0:3]]),
  squish([x[6:9] for x in board[0:3]]),
  squish([x[0:3] for x in board[3:6]]),
  squish([x[3:6] for x in board[3:6]]),
  squish([x[6:9] for x in board[3:6]]),
  squish([x[0:3] for x in board[6:9]]),
  squish([x[3:6] for x in board[6:9]]),
  squish([x[6:9] for x in board[6:9]])
]

  region = all([tester(x) for x in regions])  
 # region = True
  if all([horizontal, vertical, region]):
      return 'Finished!'
  else:
      return 'Try again!'



from unittest import TestCase, main

class Test(TestCase):
    def test_done_or_not(self):
        self.assertEqual(done_or_not([
                         [1, 3, 2, 5, 7, 9, 4, 6, 8]
                        ,[4, 9, 8, 2, 6, 1, 3, 7, 5]
                        ,[7, 5, 6, 3, 8, 4, 2, 1, 9]
                        ,[6, 4, 3, 1, 5, 8, 7, 9, 2]
                        ,[5, 2, 1, 7, 9, 3, 8, 4, 6]
                        ,[9, 8, 7, 4, 2, 6, 5, 3, 1]
                        ,[2, 1, 4, 9, 3, 5, 6, 8, 7]
                        ,[3, 6, 5, 8, 1, 7, 9, 2, 4]
                        ,[8, 7, 9, 6, 4, 2, 1, 5, 3]]), 'Finished!')
       
       
if __name__=="__main__":main()
