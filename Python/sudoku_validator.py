def done_or_not(board):
    for i in xrange(0, 9):
      (l, c, b) = (set(), set(), set())
      for j in xrange(0, 9):
          c.add(board[i][j]) 
          l.add(board[j][i])
          b.add(board[0 + j / 3][((i * 3) % 9) + j % 3])
      if len(l)!=9 or len(c)!=9 or len(b)!=9:
          return 'Try again!'
    return "Finished!"

********************************************************************
def done_or_not(board):
  rows = board
  cols = [map(lambda x: x[i], board) for i in range(9)]
  squares = [
    board[i][j:j + 3] + board[i + 1][j:j + 3] + board[i + 2][j:j + 3]
      for i in range(0, 9, 3)
      for j in range(0, 9, 3)]
    
  for clusters in (rows, cols, squares):
    for cluster in clusters:
      if len(set(cluster)) != 9:
        return 'Try again!'
  return 'Finished!'


**********************************************************************
Mine:
**********************************************************************
def done_or_not(board): #board[i][j]
  tester = lambda x:len(set(x))==9
  # your solution here
  # ..
  # return 'Finished!'
  # ..
  # or return 'Try again!'
  horizontal = all([tester(board[i]) for i in range(9)])
  vertical = all([tester([board[i][j] for i in range(9)]) for j in range(9)])   

   
  
  def squish(r):
      ml = []
      for tri in r:
          for item in tri:
              ml.append(item)
      return ml
      
  

  regions = [
  squish([x[0:3] for x in board[0:3]]),
  squish([x[3:6] for x in board[0:3]]),
  squish([x[6:9] for x in board[0:3]]),
  squish([x[0:3] for x in board[3:6]]),
  squish([x[3:6] for x in board[3:6]]),
  squish([x[6:9] for x in board[3:6]]),
  squish([x[0:3] for x in board[6:9]]),
  squish([x[3:6] for x in board[6:9]]),
  squish([x[6:9] for x in board[6:9]])
]

  region = all([tester(x) for x in regions])  
  
  if all([horizontal,vertical, region]):
      return 'Finished!'
  else:
      return 'Try again!'
