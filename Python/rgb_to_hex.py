def rgb(r, g, b):
    ''' return a hex value for the given RGB value. '''
    hex = [str(x) for x in range(10)] + ['A','B','C','D','E','F']
    conv_dict = {x:hex[x] for x in range(16)}
    result = []
    for x in [r, g, b]:
        if x >= 255: result.append('FF')
        elif x <= 0: result.append('00')
        else:
            result.append(conv_dict[divmod(x,16)[0]])
            result.append(conv_dict[divmod(x,16)[1]])
    return ''.join(result)





from unittest import TestCase, main

class RGBTest(TestCase):
    def test_rgb_zero_values(self):
        self.assertEquals(rgb(0,0,0),"000000")

    def test_rgb_near_zero_values(self):
        self.assertEquals(rgb(1,2,3),"010203")

    def test_rgb_max_values(self):
        self.assertEquals(rgb(255,255,255), "FFFFFF")

    def test_rgb_near_max_values(self):
        self.assertEquals(rgb(254,253,252), "FEFDFC")

    def test_rgb_out_of_range_values(self):
        self.assertEquals(rgb(-20,275,125), "00FF7D")
        

if __name__ == "__main__":main()
