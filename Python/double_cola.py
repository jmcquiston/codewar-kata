def whoIsNext(names, r):
    x = len(names)
    i = 1
    while r > x:
        r -= x
        x *= 2
        i*=2   
    result = divmod(r,i)
    return names[divmod(r,i)[0]] if result[1] else names[divmod(r,i)[0]-1]


def double(l):
    ''' move first member to end and double '''
    d = l.pop(0)
    l.append(d)
    l.append(d)





from unittest import TestCase, main

class TestWhoIs(TestCase):
    def test_who_is_next(self):
        names = ["Sheldon", "Leonard", "Penny", "Rajesh", "Howard"]
        self.assertEqual(whoIsNext(names, 1),"Sheldon")
        self.assertEqual(whoIsNext(names, 52),"Penny")
        self.assertEqual(whoIsNext(names, 14), "Howard")
        self.assertEqual(whoIsNext(names,7230702951),"Leonard")
        
                
if __name__ == "__main__":main()








