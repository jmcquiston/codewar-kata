from itertools import product

def count_change(money, coins):
    ''' Calculate how many ways to total money using given coins. '''
    coins=sorted(coins, reverse=True)
    partial_array_list = partial_array_generator(money, coins)
    result = 0
    while True:
        try:
            if array_validator(money, coins, partial_array_list.next()):
                result += 1
        except:
            return result


def partial_array_generator(money, coins):
    ''' Return all combinations of coin values excluding the last. '''
    return product(*get_list_multiples(money,coins[:-1]))

    
def get_list_multiples(total,coins):
    '''Return a list of lists of multiples, uses get_list_multiples'''
    a=[]
    for multiple in [get_multiples(total, coins[x]) for x in range(len(coins))]:
        a.append(multiple)
    return a

    
def get_multiples(total,denom):
    '''Return a list of multiples of the denomination, each within bounds of total'''
    return[(denom*x) for x in range(total/denom+1)]


def array_validator(money, coins, array):
    ''' Check that, given an array excluding last value, array will add to total exactly '''
    if ((money-sum(array)) % coins[-1]==0) and (money>=sum(array)):
        return True
    else:
        return False


from unittest import TestCase, main
class Test(TestCase):
    def test_count_change(self):
        self.assertEquals(3, count_change(4, [1,2]))

    def test_count_change2(self):
        self.assertEquals(2, count_change(10, [2,3]))

    def test_count_change3(self):
        self.assertEquals(0, count_change(11, [5,7]))

    def test_count_change4(self):
        self.assertEquals(1, count_change(11, [5,7,4]))

    def test_count_change5(self):
        self.assertEquals(242, count_change(100, [1,5,10,25]))
        
    def test_count_change6(self):
        self.assertEquals(25, count_change(400, [18,3,53]))
        
    def test_count_change7(self):
        self.assertEquals(91, count_change(999, [9,22,33]))
        
    def test_count_change8(self):
        self.assertEquals(10635, count_change(75, [1,3,4,6,7,9]))


if __name__ == "__main__":main()





