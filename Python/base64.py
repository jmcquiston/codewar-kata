


def base64_to_base10(str):
    base = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    base10 = 0
    for char in range(len(str)):
        revindex = len(str)-char-1
        char_value = base.index(str[char])
        base10+= char_value*64**revindex
    return base10


from unittest import TestCase, main

class Test(TestCase):
    def test_one(self):
        self.assertEquals(base64_to_base10("A"  ), 0    )
        self.assertEquals(base64_to_base10("/"  ), 63   )
        self.assertEquals(base64_to_base10("BA" ), 64   )
        self.assertEquals(base64_to_base10("//" ), 4095 )
        self.assertEquals(base64_to_base10("WIN"), 90637)


if __name__ == "__main__": main()



