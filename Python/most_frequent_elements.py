def find_most_frequent(l):
    ''' Find most frequent element in list '''
    try:
        max_number = l.count(max(l,key=lambda x:l.count(x)))
    except ValueError:
        return set([])
    matching_items = filter(lambda x:l.count(x)==max_number,l)
    return set(matching_items)
    
    


from unittest import TestCase, main

class Test(TestCase):
	def test_find_most_frequent(self):
	    self.assertEqual(find_most_frequent([1, 1, 2, 3]), set([1]))

	    self.assertEqual(find_most_frequent([1, 1, 2, 2, 3]),set([1, 2]))

	    self.assertEqual(find_most_frequent([1, 1, '2', '2', 3]), set([1, '2']))

	    self.assertEqual(find_most_frequent([]), set([]))

if __name__ == '__main__':main()
