
def solve(map, miner, exit):
    out_of_bounds = lambda pos:all([pos["x"] in range(0,len(map)),pos["y"] in range(0,len(map[0]))])
                                   
    def possible_moves(miner, previous=[]):
        if miner==exit:return []
        else:
            directions = ["right","left","down", "up"]
            for direction in directions:
                #if there are any moves to be made,
                new_position = move(miner, direction)
                if new_position not in previous + blocked(map) and out_of_bounds(new_position):
                    print new_position
                    previous.append(new_position)
                    if new_position==exit:
                        return convert_to_directions(previous)
                    else:
                        pm = possible_moves(new_position, previous)
                        if pm: return pm
    
    answer = possible_moves(miner, [miner])
    return answer

def convert_to_directions(path):
    result =[]
    pos_to_dir = {'x':{1:"right",-1:"left"},"y":{1:"down",-1:"up"}}
    start = path[0]
    for position in path[1:]:
        x_change = position['x']-start["x"]
        if x_change:
            result.append(pos_to_dir["x"][x_change])
            start = position
        else:
            y_change = position['y']-start["y"]
            result.append(pos_to_dir["y"][y_change])
            start = position   
    return result
        
    
def blocked(map):
    result = []
    for column in enumerate(map):
        for row in enumerate(column[1]):
            if not row[1]:
                result.append({'y':row[0],'x':column[0]})
    return result
            
            

def move(pos, direction):
    dirs = {"right":("x",1),
            "left":("x",-1),
            "up":("y",-1),
            "down":("y",1)}
    dir = dirs[direction]
    coordinate = dir[0]
    not_coord = {"x":"y","y":"x"}[coordinate]
    return {coordinate:pos[dir[0]]+dir[1], not_coord:pos[not_coord]}

from unittest import TestCase, main

class Test(TestCase):
    def test_blocked(self):
        minemap = [[True, True, True],
  [False, False, True],
  [True, True, True]]
        self.assertEquals(blocked(minemap), [{'x':1,'y':0},{'x':1,'y':1}])

    def test_convert_to_directions(self):
        path = [{'x':0,'y':0},{'x':1,'y':0},{'x':2,'y':0},{'x':2,'y':1},{'x':1,'y':1},{'x':1,'y':2}]
        self.assertEquals(convert_to_directions(path), ['right', 'right', 'down', 'left', 'down'])

    def test_solve(self):
        minemap = [[True, True, True],
  [False, False, True],
  [True, True, True]]
        self.assertEquals(solve(minemap, {'x':0,'y':0}, {'x':2,'y':0}), ['down', 'down', 'right', 'right', 'up', 'up'])
        minemap = [[True,True, True, False, False],[False, False, True, False, True],[True, False, True, False, True],
                   [True, False, True, False, False],[False, True, True, True, True]]
        self.assertEquals(solve(minemap, {"x":0,"y":0}, {"x":4,'y':4}),['down', 'down', 'right', 'right', 'right', 'right', 'down', 'down'])

if __name__=="__main__": main()
