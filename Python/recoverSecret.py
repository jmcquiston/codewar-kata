def recoverSecret(triplets):
   ''' find secret string given a set of triplets that show relative order of those letters within secret string. '''
   unsorted_list = []
   for x in triplets: unsorted_list += x
   ''' Run list through the reduce/resort process a few times to weed out any anomalies. '''
   result = reduce(resort, triplets,list(set(unsorted_list)))
   result = reduce(resort, triplets,result)
   return ('').join(reduce(resort, triplets, result))

    
def resort(long,short):
   ''' Resort the letters of the given triplet within the larger list. '''
   index_array = [long.index(short[0]),long.index(short[1]),long.index(short[2])]
   index_array = sorted(index_array)
   for i in range(3):
      long[index_array[i]] = short[i]
   return long 





    
from unittest import TestCase, main


class Test(TestCase):

   def test_recoverSecret(self):
      secret1 = "whatisup"
      triplets1 = [
        ['t','u','p'],
        ['w','h','i'],
        ['t','s','u'],
        ['a','t','s'],
        ['h','a','p'],
        ['t','i','s'],
        ['w','h','s']
      ]

      self.assertEquals(recoverSecret(triplets1), secret1)

if __name__ == "__main__":main()
