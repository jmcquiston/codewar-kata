import re
def compare(a,b):
    """find selector with high specificity rating"""
    sel_ary1 = get_selector_array(a)
    sel_ary2 = get_selector_array(b)
    if sel_ary1[0] != sel_ary2[0]:
      return a if sel_ary1[0] > sel_ary2[0] else b
    elif sel_ary1[1] != sel_ary2[1]:
        return a if sel_ary1[1] > sel_ary2[1] else b
    else:
        return a if sel_ary1[2] > sel_ary2[2] else b

def get_selector_array(selector):
    """determine the specificity rating as an array of (ids, classes, elements)"""
    ids = selector.count("#")
    classes = selector.count(".")
    elements = len(re.findall(r' \w+|^\w+', selector))
    return (ids, classes, elements)



from unittest import TestCase, main

class CompareSelectors(TestCase):

    def test_compare(self):
        self.assertEquals(compare("body p", "div"),"body p")
        self.assertEquals(compare(".class", "#id"),"#id")
        self.assertEquals(compare("div.big", ".small"),"div.big")
        self.assertEquals(compare(".big", ".small"),".small")
