

'''
General rules

Rules of bowling in a nutshell:

A game consists of 10 frames. In each frame the player rolls 1 or 2 balls, except for the 10th frame, where the player rolls 2 or 3 balls.
The total score is the sum of your scores for the 10 frames
If you knock down fewer than 10 pins with 2 balls, your frame score is the number of pins knocked down
If you knock down all 10 pins with 2 balls (spare), you score the amount of pins knocked down plus a bonus - amount of pins knocked down with the next ball
If you knock down all 10 pins with 1 ball (strike), you score the amount of pins knocked down plus a bonus - amount of pins knocked down with the next 2 balls
Rules for 10th frame

As the 10th frame is the last one, in case of spare or strike there will be no next balls for the bonus. To account for that:

if the last frame is a spare, player rolls 1 bonus ball.
if the last frame is a strike, player rolls 2 bonus balls.
These bonus balls on 10th frame are only counted as a bonus to the respective spare or strike.

'''


spare_strike = []        
def bowling_score_0(rolls):
    del spare_strike[:]
    score = []
    for i in range(10):
        turn(rolls, score)
    while rolls:
        turn(rolls, score)
    bonuses = [bonus(mark, score) for mark in enumerate(spare_strike[:10])]
    return sum(filter(lambda x:x!='F', score[:20]))+sum(bonuses)

def bonus(mark, score):
    if mark[1] == 'none':
        return 0
    #for a spare, the next throw counts twice.
    elif mark[1] == 'spare':
        i = (mark[0]+1)*2
        return score[i]
    #for a strike, the next 2 throw counts twice.
    else:
        #mark[0] is the frame index.  strikes count the next two throws, but it's adding in a -1 to the score
        if mark[0]<10:
            #i is the index of the next throw
            i = (mark[0]+1)*2
            ans = []
            ans.append(score[i]) if score[i]>=0 else ans.append(score[i+1])
            ans.append(score[i+1]) if score[i+1]!='F' else ans.append(score[i+2])
            ans = filter(lambda x:x!='F', ans)
            return sum(ans)
        else:
            return 0
                     
        
def turn(rolls, score):
    spare = rolls[0]<10 and sum(rolls[0:2])==10
    spare_strike.append("spare") if spare else spare_strike.append('none')
    if rolls[0]==10:
        spare_strike[-1]="strike"
        score.extend([rolls.pop(0),'F'])
    else:
        if len(rolls)>1:
            score.extend([rolls.pop(0),rolls.pop(0)])
        else:
            score.extend([rolls.pop(0)])

''' ================================================================================================'''
            
def bowling_score_1(rolls):
  "Compute the total score for a player's game of bowling."
  
  def is_spare(rolls):
    return 10 == sum(rolls[:2])

  def is_strike(rolls):
    return 10 == rolls[0]

  def calc_score(rolls, frame):
    return (sum(rolls) if frame == 10 else
            sum(rolls[:3]) + calc_score(rolls[1:], frame+1) if is_strike(rolls) else
            sum(rolls[:3]) + calc_score(rolls[2:], frame+1) if is_spare(rolls) else
            sum(rolls[:2]) + calc_score(rolls[2:], frame+1))
  
  return calc_score(rolls,1)

def bowling_score(rolls):
    count = 0
    score = 0
    for count in range(10):# count < 10:
        #count += 1
        frame = rolls.pop(0)
        if frame < 10:
            frame += rolls.pop(0)
            score += frame
            if frame == 10:
                score += rolls[0]
        else:
            score += frame + rolls[0] + rolls[1]
    return score




def bowling_score_3(rolls):
  tot,i = 0,0
  for frm in range(10):
      fscr,i = rolls[i],i+1
      if fscr == 10:
          tot += fscr + rolls[i] + rolls[i+1]
          continue
      fscr += rolls[i]
      i += 1
      if fscr >= 10:
          tot += rolls[i]
      tot += fscr
  return tot





from unittest import TestCase, main

class Test(TestCase):
    def test_one(self):
        self.assertEquals(1,1)

        self.assertEquals( 95, bowling_score( [1,4,5,5,5,4,3,7,2,3,7,2,1,5,6,2,4,3,8,2,9] ) )
        self.assertEquals( 12 , bowling_score([1,9,1] + [0]* 17) )
        self.assertEquals( 95 , bowling_score([1,4,5,5,5,4,3,7,2,3,7,2,1,5,6,2,4,3,8,2,9]) )

        self.assertEquals( 300, bowling_score( [10]*12 ) )
        self.assertEquals( 11, bowling_score([0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 10,1,0]) )
        self.assertTrue( 12 == bowling_score([0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 10, 1,0]) )


if __name__=="__main__":main()
